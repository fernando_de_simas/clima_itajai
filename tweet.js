require('dotenv').config();
var request = require('request');
var Twitter = require('twitter');
var data = require('node-datetime');
let city = process.env.CITY_ID;
let appkey = process.env.OPENWEATHER_KEY;
let OPEN_WEATHER_URL = `http://api.openweathermap.org/data/2.5/weather?id=${city}&lang=pt&APPID=${appkey}&units=metric`;

request(OPEN_WEATHER_URL, function (err, response, body) {
    if (err) {
        console.log(error);
    } else {
        ret = JSON.parse(body);
        dt = data.create();
        dtTweet = dt.format('H:M d/m/y');
        tweet = { status: `Faz ${ret.main.temp} graus em ${ret.name} às ${dtTweet}. Condição: ${ret.weather[0].description}` };
        console.log(tweet);
        let client = new Twitter({
            consumer_key: process.env.consumer_key,
            consumer_secret: process.env.consumer_secret,
            access_token_key: process.env.access_token_key,
            access_token_secret: process.env.access_token_secret
        });
        client.post('statuses/update', tweet, function (error, tweet, response) {
            if (error) throw error;
            console.log("Tweet enviado com sucesso.");  // Tweet body.            
        });
    }
})